/* Відповідь 1: 
Прототипне наслідування: механізм, який дозволяє об'єктам ділитися своїми властивостями та методами.
Кожен об'єкт має приховану властивість [[Prototype]], яка посилається на інший об'єкт, який називається прототипом.
Прототип, може мати власний прототип, і так далі, поки не буде досягнуто об'єкта з прототипом null.*/

/* Відповідь 2:
Щоб ініціалізувати властивості та методи батьківського класу.
Якщо не викликати super() у конструкторі класу-нащадка в консолі побачимо undefined.*/

// Завдання :

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }

  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }
  set lang(newLang) {
    this._lang = newLang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmerFirst = new Programmer("Vitaliy", 24, 4000, [
  "JavaScript",
  "ReactJS",
]);
const programmerSecond = new Programmer("Vlad", 31, 5500, ["Python", "C++"]);

console.log(programmerFirst);
console.log(programmerSecond);
